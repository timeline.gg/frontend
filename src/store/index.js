import Vue from 'vue'
import Vuex from 'vuex'
import user from "./modules/user"
import auth from "./modules/auth"
import category from "./modules/category"
import card from "./modules/card"
import game from "./modules/game"

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    auth,
    category,
    card,
    game
  }
})
