
const getters = {
    isAuthenticated: state => {
        return !!state.token && (new Date() < new Date(state.expires_at))
    },
    getAuthStatus: state => state.status,
    getAuthUser: state => state.user,

    isAdmin: state => {
        if ( state.user && "roles" in state.user) {
            return state.user.roles.filter(e => e.name=='admin').length == 1;
        } else { 
            return false;
        }
    }
}

export default getters