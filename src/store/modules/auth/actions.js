//Importo el objeto "http" para su uso en las promesas.
import http from "@/utils/http";

const actions = {
  user: ({commit}) => {
    return new Promise((resolve, reject) => {
      http
        .get('api/auth/user')
        .then(resp => {          
          commit("SET_USER", resp.data.data)
          resolve(resp)
        })
        .catch(error => {
          reject(error.response.data)
        })
      });
  },
  signup: ({ commit }, data) => {
    
    return new Promise((resolve, reject) => {
      http
        //Ruta que ejecuta la función en laravel
        .post('api/auth/signup', data)
        //Código que se ejecuta en caso de "RESOLVE". el argumento de (then) es una función que tiene el argumento "resp" que es un argumento de axios
        .then(resp => {
          //hago commit, le paso la mutación 
          commit("AUTH_SUCCESS", resp.data);//data es el json que me devuelve laravel. Resp es el response schema de axios
          resolve(true)       
        })
        //Código que se ejecuta en caso de "REJECT".
        .catch(error => {
          console.log(error)
          reject(error.response.data)
        })
    });
  },

  activate: ({ commit }, data) => {
    
    return new Promise((resolve, reject) => {
      commit("ACTIVATION_REQUEST");
        http
        //Ruta que ejecuta la función en laravel
        .get('api/auth/signup/activate/' + data.token)
        //Código que se ejecuta en caso de "RESOLVE". el argumento de (then) es una función que tiene el argumento "resp" que es un argumento de axios
        .then(resp => {          
          console.log(resp)
          //hago commit, le paso la mutación 
          commit("ACTIVATION_SUCCESS");//data es el json que me devuelve laravel. Resp es el response schema de axios
          resolve(true)       
        })
        //Código que se ejecuta en caso de "REJECT".
        .catch(error => {
          commit("ACTIVATION_ERROR")
          reject(error.response.data)
        })
    });
  },


  //Función de login llaves hacen destructuring (desempaquetado)
  login: ({ commit }, data) => {
    
    return new Promise((resolve, reject) => {
      http
        //petición para obtener los datos(token...)
        .post('api/auth/login', data)
        //Código que se ejecuta en caso de "RESOLVE". el argumento de (then) es una función que tiene el argumento "resp" que es un argumento de axios
        .then(resp => {
          //accedo a la store del navegador y con setItem puedo obtener token. se actualiza en el navegador el token
          localStorage.setItem("_token", resp.data.access_token);
          localStorage.setItem("_expires_at", resp.data.expires_at);
          //hago commit, le paso la mutación 
          commit("AUTH_SUCCESS", resp.data);//data es el json que me devuelve laravel. Resp es el response schema de axios
          resolve(true)       
        })
        //Código que se ejecuta en caso de "REJECT".
        .catch(error => {
          reject(error.response.data)
        })
    });
  },
  //Mi función de logout (pide al servidor que haga logout)
  logout: ({ commit }) => {
    return new Promise((resolve, reject) => {
      http
        .get('api/auth/logout')
        .then(resp => {
          localStorage.removeItem("_token");
          localStorage.removeItem("_expires_at");
          commit("AUTH_LOGOUT");
          resolve(true)
        })
        //REJECT
        .catch(error => {
          if (error.response) {
            if(error.response.status == 401){
              localStorage.removeItem("_token");
              localStorage.removeItem("_expires_at");
              commit("AUTH_LOGOUT");
              resolve(true)
            }
          }
          reject(error.response.data)
        })
    });
  }


}

export default actions