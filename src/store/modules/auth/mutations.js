const mutations = {
  ACTIVATION_REQUEST: state => {
    state.status = "activation_request";
  },
  ACTIVATION_SUCCESS: state => {
    state.status = "activation_success";
  },
  ACTIVATION_ERROR: state => {
    state.status = "activation_error";
  },
  AUTH_REQUEST: state => {
    state.status = "loading";
  },
  AUTH_SUCCESS: (state, resp) => {
    state.status = "success";
    state.token = resp.access_token;
    state.expires_at = resp.expires_at;
    state.user = resp.user;
    state.hasLoadedOnce = true;
  },
  AUTH_ERROR: state => {
    state.status = "error";
    state.hasLoadedOnce = true;
  },
  AUTH_LOGOUT: state => {
    state.token = "";
    state.user = null;
    state.expires_at = null;
  },
  SET_USER: (state, user) => {
    state.user = user;
  }
};

export default mutations