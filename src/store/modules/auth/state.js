const state = {
    user: null,
    token: localStorage.getItem("_token") || "",
    expires_at:  localStorage.getItem("_expires_at") || null,
    status: "",
    hasLoadedOnce: false
  };

export default state