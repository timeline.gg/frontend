import Vue from 'vue'

const mutations = {
    REQUEST_START: (state, status) => {
      state.status = status
      state.isLoading = true
    },     
    REQUEST_ERROR: state => {
      state.status = "error"
      state.isLoading = false
    },
    REQUEST_SUCCESS: state => {
      state.status = ""
      state.isLoading = false
    }
  };

export default mutations
  