//Importo el objeto "http" para realizar la petición http.
import http from "@/utils/http";

const actions = {

    delete: ({commit}, {id}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "delete");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .delete('api/cards/' + id)
              .then(resp => {                
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      });
      
    },
    show: ({commit}, {id}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "show");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .get('api/cards/' + id)
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      });
      
    },
    store: ({commit}, {formData}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "store");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .post('api/cards', formData)
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      })
    },
    update: ({commit}, {id, formData}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "update");
      formData.append("_method", "PUT")
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .post('api/cards/'+id, formData)
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      })
    }
    
  };

  export default actions