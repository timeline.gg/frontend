const getters = {
    getProfile: state => state.profile,
    isProfileLoaded: state => !!state.profile.name,
    isLoading: state => state.status == "loading"
  };

  export default getters