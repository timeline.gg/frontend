const state = {
    //Defino la propiedad "status" que podrá variar --> 'loading', 'succes', 'error'
    status: "",
    //Defino la propiedad "profile", en donde tendré el array de datos del módulo 'User' (es decir, los datos del usuario según id).
    profile: null
};

export default state
