//Importo el objeto "http" para realizar la petición http.
import http from "@/utils/http";

const actions = {
    //Acción (función) 'profile'. Ésta realiza la petición mediante Axios a Laravel para poder obtener al usaurio
    show: ({commit}, user) => {
      //Lo primero es comitear "USER_REQUEST" para actualizar el 'status' (loading)
      commit("REQUEST_START", "update");      //Ejecuto la promesa que realiza la petición http para obtener al usuario.
      return new Promise((resolve, reject) => {
        http
          //.get me hace la petición. Le paso la ruta y el id del usuario.
          .get('api/users/' + user)
          .then(resp => {
            //En este momento hago commit de "USER_SUCCES" para actualizar el estado a 'succes'. Ha sucedido (se ha resuelto)
            commit("REQUEST_SUCCESS")
            resolve(resp.data)
          })
          .catch(error => {
            commit("REQUEST_ERROR")
            reject(error.response.data)
          })
        });
    },

    update: ({commit}, {user, formData}) => {
      
      commit("REQUEST_START", "update");
      //Ejecuto la promesa que realiza la petición http 
      return new Promise((resolve, reject) => {                 
            formData.append("_method", "PATCH")        
            http
              //.get me hace la petición. Le paso la ruta y el id del usuario.
              .post('api/users/' + user, formData)
              .then(resp => {
                //En este momento hago commit de "USER_SUCCES" para actualizar el estado a 'succes'. Ha sucedido (se ha resuelto)
                commit("REQUEST_SUCCESS")
                resolve(resp.data)
              })
              .catch(error => {
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      });
      
    },

    index: ({commit}, {perPage, page, filter}) => {
      //Comit para el estado (cargando)
      commit("REQUEST_START", "index");     
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .get('api/users', { params: {perPage, page, filter}})
              .then(resp => {
                //Commit de "INDEX_SUCCES" para actualizar el estado a 'succes'.
                commit("REQUEST_SUCCESS")                
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      });
      
    },
    delete: ({commit}, {id}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "delete");
      return new Promise((resolve, reject) => {                 
            http
              //Petición.
              .delete('api/users/' + id)
              .then(resp => {                
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      });
      
    }
  };

  export default actions