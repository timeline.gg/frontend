const state = {
    //Defino la propiedad "status" que podrá variar --> 'loading', 'succes', 'error'
    isLoading: false,
    status: ""    
};

export default state
