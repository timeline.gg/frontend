//Importo el objeto "http" para realizar la petición http.
import http from "@/utils/http";

const actions = {
    //Acción (función) 'cards'. Ésta realiza la petición mediante Axios a Laravel para poder obtener las categorías
    categories: ({commit}) => {
      //Comit para el estado (cargando)
      commit("REQUEST_START", "game.categories");     
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .get('api/game/categories')
              .then(resp => {
                //Commit de "INDEX_SUCCES" para actualizar el estado a 'succes'.
                commit("REQUEST_SUCCESS")                
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error)
              })
      });
    },
    start: ({commit}, {category_id}) => {
      //Comit para el estado (cargando)
      commit("REQUEST_START", "start");     
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .get('api/game/start/'+category_id)
              .then(resp => {
                //Commit de "INDEX_SUCCES" para actualizar el estado a 'succes'.
                commit("REQUEST_SUCCESS")                
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error)
              })
      });
      
    },
    hand: ({commit}, {id}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "game.cards");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .get('api/game/' + id + '/hand')
              .then(resp => {                
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error)
              })
      });
      
    },
    resolve: ({commit}, {id, params}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "game.resolve");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .post('api/game/' + id+ '/resolve',params)
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error)
              })
      });
      
    },
    store: ({commit}, {formData}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "store");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .post('api/categories', formData)
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error)
              })
      })
    },
    update: ({commit}, {id, formData}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "update");
      formData.append("_method", "PUT")
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .post('api/categories/'+id, formData)
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error)
              })
      })
    }
    
  };

  export default actions