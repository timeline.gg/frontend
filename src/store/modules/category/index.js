//Importo los módulos
import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
//Para hacer uso de 'namespaced' pongo a true el atributo namespaced en la raíz del módulo.
//Al hacer uso de namespaced para acceder al getter o estado debo usa la siguiente forma: this.$store.getters['...'];
const namespaced = true

export default {
    namespaced,
    state,
    getters,
    mutations,
    actions
}
