//Importo el objeto "http" para realizar la petición http.
import http from "@/utils/http";

const actions = {
    //Acción (función) 'cards'. Ésta realiza la petición mediante Axios a Laravel para poder obtener las categorías
    cards: ({commit}, {id, perPage, page, sortBy, sortDesc, filter}) => {
      //Comit para el estado (cargando)
      commit("REQUEST_START", "cards");     
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .get('api/categories/'+id+'/cards', { params: {perPage, page, sortBy, sortDesc, filter}})
              .then(resp => {
                //Commit de "INDEX_SUCCES" para actualizar el estado a 'succes'.
                commit("REQUEST_SUCCESS")                
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error)
              })
      });
    },
    index: ({commit}, {perPage, page, sortBy, sortDesc}) => {
      //Comit para el estado (cargando)
      commit("REQUEST_START", "index");     
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .get('api/categories', { params: {perPage, page, sortBy, sortDesc}})
              .then(resp => {
                //Commit de "INDEX_SUCCES" para actualizar el estado a 'succes'.
                commit("REQUEST_SUCCESS")                
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error)
              })
      });
      
    },
    delete: ({commit}, {id}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "delete");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .delete('api/categories/' + id)
              .then(resp => {                
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      });
      
    },
    show: ({commit}, {id}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "show");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .get('api/categories/' + id)
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      });
      
    },
    store: ({commit}, {formData}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "store");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .post('api/categories', formData)
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      })
    },
    update: ({commit}, {id, formData}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "update");
      formData.append("_method", "PUT")
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .post('api/categories/'+id, formData)
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      })
    },
    rankings: ({commit}) => {
      //Comit para el estado (cargando)      
      commit("REQUEST_START", "rankings");
      return new Promise((resolve, reject) => {                 
            http
              //Petición. Ruta y paginación
              .get('api/rankings')
              .then(resp => {     
                commit("REQUEST_SUCCESS")                           
                resolve(resp.data)
              })
              .catch(error => {
                 //En caso de error commit de "REQUEST_ERROR" para poner el estado de error
                commit("REQUEST_ERROR")
                reject(error.response.data)
              })
      })
    },
    
  };

  export default actions