const getters = {
    isLoading: state => state.isLoading,
    getStatus: state => state.status
  };

  export default getters