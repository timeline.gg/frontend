import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import store from './store'

import CategoriesIndex from './views/admin/categories/Index.vue'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters['auth/isAuthenticated']) {
    next();
    return;
  }
  next("/");
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters['auth/isAuthenticated']) {
    if (!store.getters['auth/getAuthUser']) {
      store.dispatch('auth/user')
        .then(resp => next())
        .catch(error => next('/login'))
    } else {
      next()
    }
  } else {
    next('/login')
  }
};

const ifAdmin = (to, from, next) => {
  if (store.getters['auth/isAuthenticated']) {
    if (!store.getters['auth/getAuthUser']) {
      store.dispatch('auth/user')
        .then(resp => {
          if (store.getters['auth/isAdmin']) {
            next()
          } else {
            next('/login')
          }
        })
        .catch(error => next('/login'))
    } else {
      if (store.getters['auth/isAdmin']) {
        next()
      } else {
        next('/login')
      }
    }
  } else {
    next('/login')
  }
};

export default new Router({
  mode: 'history',
  // base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      meta: {
        title: 'TimeLine',
      },
      component: Home
    },
    {
      path: '/registro',
      name: 'register',
      meta: {
        title: 'Registro',
      },
      beforeEnter: ifNotAuthenticated,
      component: () => import(/* webpackChunkName: "register" */ './views/Register.vue')
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        title: 'Iniciar sesion',
      },
      beforeEnter: ifNotAuthenticated,
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue')
    },
    {
      path: '/logout',
      name: 'logout',
      meta: {
        title: 'Cerrar sesión',
      },
      beforeEnter: ifAuthenticated,
      component: () => import(/* webpackChunkName: "logout" */ './views/Logout.vue')
    },
    {
      path: '/rules',
      name: 'rules',
      meta: {
        title: 'Reglas',
      },
      component: () => import(/* webpackChunkName: "rules" */ './views/Rules.vue')
    },
    {
      path: '/account',
      name: 'account',
      meta: {
        title: 'Mi cuenta',
      },
      beforeEnter: ifAuthenticated,
      component: () => import(/* webpackChunkName: "account" */ './views/Account.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      meta: {
        title: 'Perfil',
      },
      beforeEnter: ifAuthenticated,
      component: () => import(/* webpackChunkName: "profile" */ './views/Profile.vue')
    },
    {
      path: '/game',
      name: 'game',
      meta: {
        title: 'Jugar',
      },
      component: () => import(/* webpackChunkName: "game" */ './views/Game.vue')
    },
    {
      path: '/game/:category_id',
      name: 'game.start',
      props: true,
      meta: {
        title: 'Jugar',
      },
      component: () => import(/* webpackChunkName: "game" */ './views/GameStart.vue')
    },
    {
      path: '/activation',
      name: 'activation',
      meta: {
        title: 'activation',
      },
      component: () => import(/* webpackChunkName: "activation" */ './views/Activation.vue')
    },
    {
      path: '/admin',
      name: 'admin',
      meta: {
        title: 'admin',
      },
      beforeEnter: ifAdmin,
      component: () => import(/* webpackChunkName: "admin" */ './views/Admin.vue')
    },
    {
      path: '/ranking',
      name: 'ranking',
      meta: {
        title: 'Ranking',
      },
      component: () => import(/* webpackChunkName: "ranking" */ './views/Ranking.vue')
    },
    {
      path: '/admin/game',
      name: 'admin.game.category.index',
      meta: {
        title: 'Administrar categorías',
      },
      beforeEnter: ifAdmin,
      component: () => import(/* webpackChunkName: "admin.game.category.index" */ './views/admin/categories/Index.vue')
    },
    {
      path: '/admin/game/category/:category_id',
      component: ()  => import('./views/admin/categories/Category.vue'),
      children: [
        {
          path: '',
          name: 'admin.game.category.edit',
          props: true,          
          meta: {
            title: 'Editar categorías',
          },
          component: () => import(/* webpackChunkName: "admin.game.category.edit" */ './views/admin/categories/Edit.vue')
        },
        {
          path: 'cards',
          component: ()  => import('./views/admin/cards/Card.vue'),
          children: [
            {
              path: '',
              name: 'admin.game.category.card.index',
              props: true,
              meta: {
                title: 'Gestionar Cartas',
              },
              component: () => import(/* webpackChunkName: "admin.game.category.card.index" */ './views/admin/cards/Index.vue')
            },
            {
              path: 'create',
              name: 'admin.game.category.card.create',
              props: true,
              meta: {
                title: 'Crear cartas',
              },
              component: () => import(/* webpackChunkName: "admin.game.category.card.create" */ './views/admin/cards/Create.vue')
            },
            {
              path: ':card_id',
              name: 'admin.game.category.card.edit',
              props: true,
              meta: {
                title: 'Editar cartas',
              },
              component: () => import(/* webpackChunkName: "admin.game.category.card.edit" */ './views/admin/cards/Edit.vue')
            }
          ]
         
        }
      ]
    },
    {
      path: '/admin/game/category/create',
      name: 'admin.game.category.create',
      meta: {
        title: 'Crear categorías',
      },
      beforeEnter: ifAdmin,
      component: () => import(/* webpackChunkName: "admin.game.category.create" */ './views/admin/categories/Create.vue')
    },
    {
      path: '/admin/user',
      name: 'admin.user.index',
      meta: {
        title: 'Administrar usuarios',
      },
      beforeEnter: ifAdmin,
      component: () => import(/* webpackChunkName: "admin.user.index" */ './views/admin/user/Index.vue')
    }
  ]
})
