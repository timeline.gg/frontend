// https://raw.githubusercontent.com/sqreen/vue-authentication-example/master/src/utils/api.js

const mocks = {
    "auth": { POST: { token: "This-is-a-mocked-token" } },
    "user/me": { GET: { name: "doggo", title: "sir" } }
  };

const apiCall = ({ url, method, data }) =>
new Promise((resolve, reject) => {
    setTimeout(() => {
    try {
        if ( url == "auth" && method == 'POST') {
            if (data['username'] == 'demo' && data['password'] == '1234') {
                resolve(mocks[url][method || "GET"]);
            } else {
                reject(false);
            }
        } else {
            console.log(`Mocked '${url}' - ${method || "GET"}`);
            console.log("response: ", mocks[url][method || "GET"]);
        }    
    } catch (err) {
        reject(new Error(err));
    }
    }, 1000);
});

export default apiCall;