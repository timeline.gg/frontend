import axios from 'axios';

const URL = process.env.VUE_APP_ROOT_API //'http://127.0.0.1:8081/';

const http = axios.create({
    baseURL: URL,
  });


  // Add a request interceptor
  http.interceptors.request.use((config)=> {    
    const token = localStorage.getItem('_token');
    config.headers.Authorization =  token ? `Bearer ${token}` : '';
    // Do something before request is sent
    return config;
  }, (error)=> {
    // Do something with request error
  
    return Promise.reject(error);
  });

  // Add a response interceptor
  http.interceptors.response.use( (response)=> {
    // Do something with response data
    return response;
  }, (error)=> {
    // Do something with response error
  
    return Promise.reject(error);
  });

  export default http;